from django.db import models


class Horarios(models.Model):
    id = models.AutoField(primary_key=True)
    zone = models.TextField()
    dia = models.TextField()
    hora = models.TextField()