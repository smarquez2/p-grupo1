from django.apps import AppConfig


class RecolectoraappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recolectoraApp'
